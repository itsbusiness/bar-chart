var config = {
    threshold: {warn: 2000, error: 5000},
    title: "TestCaseName",
    subtitle: "SubTitle",
  };

var data = [
  {
    data: [
      {target: {id: 'TS_1', name: 'TestStep 1', description: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.'}, count: 2000},
      {target: {id: 'TS_2', name: 'TestStep 2', description: 'Username_TextContent_by_test'}, count: 3000},
      {target: {id: 'TS_3', name: 'TestStep 3', description: 'Username_TextContent_by_test'}, count: 4500}],
       source: 'Search'
  },
  {
    data: [
      {target: {id: 'TS_1', name: 'TestStep 1', description: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.'}, count: 725},
      {target: {id: 'TS_2', name: 'TestStep 2', description: 'Username_TextContent_by_test'}, count: 425},
      {target: {id: 'TS_3', name: 'TestStep 3', description: 'Username_TextContent_by_test'}, count: 925}],
      source: 'Action'
  }];

var COL_ERR = "#c72844";
var COL_WARN = "#ffd42e";
var COL_ACTION ="#004C9B";
var COL_SEARCH = "#98C12C";

var UNIT_LABEL_WIDTH = 100;
var UNIT_LABEL_HEIGHT = 25;
var GUTTER_WIDTH = 25;

var chartContainer = '.chart-container';
var chartLegendContainer = '.chart-legend-container';
var chartTitleContainer = '.chart-title-container';
var chartSubtitleContainer = '.chart-subtitle-container';

var margins = {
  left: UNIT_LABEL_WIDTH,
  bottom: UNIT_LABEL_HEIGHT,
  right: GUTTER_WIDTH
};

var sizes = {
  width: 1000,
  height: 100
};

var width = sizes.width - margins.left - margins.right;
var height = sizes.height - margins.bottom;

var series = data.map(function (d) {
  return d.source;
});

var description = null;

var dataset = data.map(function (d) {
  return d.data.map(function (o, i) {
    d3.select('.chart-description')
      .append('p')
      .attr('class', 'bc-description invisible')
      .attr('id', 'yaxis' + i)
      .html(o.target.description);
    // Structure it so that your numeric axis (the stacked amount) is y
    return {
      y: o.count,
      x: o.target.name
    };
  });
});

d3.layout.stack()(dataset);

var dataset = dataset.map(function (group) {
  return group.map(function (d) {
    // Invert the x and y values, and y0 becomes x0
    return {
      x: d.y,
      y: d.x,
      x0: d.y0
    };
  });
});

var svg = d3.select(chartContainer)
  .append('svg')
  .attr('width', width + margins.left + margins.right)
  .attr('height', height + margins.bottom)
  .append('g')
  .attr('transform', 'translate(' + margins.left + ', 0)');

var units = dataset[0].map(function (d) {
  return d.y;
});

var yScale = d3.scale.ordinal()
  .domain(units)
  .rangeRoundBands([0, height], .1);

var yAxis = d3.svg.axis()
  .scale(yScale)
  .orient('left');

var xMax = d3.max(dataset, function (group) {
  var groupMax = d3.max(group, function (d) {
    return d.x + d.x0;
  });
  return groupMax;
});

var xScale = d3.scale.linear()
  .domain([0, xMax])
  .range([0, width]);

var xAxis = d3.svg.axis()
  .scale(xScale)
  .orient('bottom');

var colors = function(i) {
  return i ? COL_ACTION : COL_SEARCH;
};

var groups = svg.selectAll('g')
  .data(dataset)
  .enter()
  .append('g')
  .style('fill', function (d, i) {
    return colors(i);
  });

groups.selectAll('rect')
  .data(function (d) {return d;})
  .enter()
  .append('rect')
  .attr('x', function (d) {
    return xScale(d.x0);
  })
  .attr('y', function (d, i) {return yScale(d.y);})
  .attr('height', function (d) {return yScale.rangeBand();})
  .attr('width', function (d) {return xScale(d.x);})
  .on('mouseover', function (d) {
    var xPos = parseFloat($(this).position().left + 20);
    var yPos = parseFloat($(this).position().top);
    d3.select('#tooltip')
      .style('left', xPos + 'px')
      .style('top', yPos + 'px')
      .select('#value')
      .text(d.x);
    d3.select('#tooltip').classed('hidden', false);
  })
  .on('mouseout', function () {
    d3.select('#tooltip').classed('hidden', true);
  });

svg.append('g')
  .attr('class', 'bc-x-axis bc-axis')
  .attr('transform', 'translate(0,' + height + ')')
  .call(xAxis);

svg.append('g')
  .attr('class', 'bc-y-axis bc-axis')
  .attr('id', 'yaxis')
  .call(yAxis);

d3.select(".bc-y-axis").selectAll("text").attr("id", function(d,i) {return "yaxis" + i});

d3.select(".bc-y-axis").selectAll(".tick").on('mouseover', function (d, i) {
  var xPos = $(this).position().left + 65;
  var yPos = $(this).position().top - 10;

  $('p#yaxis' + i)
    .css('left', xPos + 'px')
    .css('top', yPos + 'px')
    .removeClass('invisible');
})
  .on('mouseout', function (d, i) {
    $('p#yaxis' + i).addClass('invisible');
  });

//Set thresholds warn and error
svg.append("line")
  .style("stroke", COL_WARN)
  .style("stroke-width", "3px")
  .attr("x1", function (d) {return xScale(config.threshold.warn);})
  .attr("y1", (sizes.height - 20))
  .attr("x2", function (d) {return xScale(config.threshold.warn);})
  .attr("y2", 0);

svg.append("line")
  .style("stroke", COL_ERR)
  .style("stroke-width", "3px")
  .attr("x1", function (d) {return xScale(config.threshold.error);})
  .attr("y1", (sizes.height - 20))
  .attr("x2", function (d) {return xScale(config.threshold.error);})
  .attr("y2", 0);

// Legend
var legendContainer = d3.select(chartLegendContainer)
  .append('div')
  .attr('class', 'bc-legend');

legendContainer
  .append('span')
  .attr('class', 'bc-legend-label')
  .html(series[0]);

series.forEach(function (s, i) {
  legendContainer.append('span')
    .attr('class', 'bc-legend-color')
    .style('background-color', colors(i));
});

legendContainer
  .append('span')
  .attr('class', 'bc-legend-label')
  .html(series[1]);

//titles
d3.select(chartTitleContainer)
  .append('h1')
  .attr('class', 'bc-title')
  .html(config.title);

d3.select(chartSubtitleContainer)
  .append('p')
  .attr('class', 'bc-subtitle')
  .html(config.subtitle);
